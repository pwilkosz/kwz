#include "graf.h"





graf::~graf(void)
{
}

void graf::dodaj_wezel(int id, int czas){
	node N(id, czas);
	wezly.push_back(N);
}

void graf::dodaj_wezel(int id, int czas, int t_min, int koszt){
	node N(id, czas, t_min, koszt);
	wezly.push_back(N);
}

void graf::usun_wezel(int id){

	for (int i = 0; i < wezly.size(); i++){
		if (i == id) wezly[i].id = -1; //zeby nie bylo juz uzywane
		for (int j = 0; j < wezly[i].krawedzie_we.size(); j++){
			if (wezly[i].krawedzie_we[j] == id){
				wezly[i].krawedzie_we.erase(wezly[i].krawedzie_we.begin() + j);
			}
		}
	}
}
void graf::polacz(int src, int dst){
	wezly[dst - 1].dodaj_krawedz_we(src - 1);
	wezly[src - 1].dodaj_krawedz_wy(dst - 1);
}

int graf::rozmiar(){
	return wezly.size();
}

int graf::wez_czas_calkowity() {
	int czas_calkowity = 0;

	for (auto it = wezly.begin(); it < wezly.end(); it++) {
		czas_calkowity = std::max(czas_calkowity, it->EF);
	}

	return czas_calkowity;
}

void graf::przypisz_wszystkim_LF_max() {
	int czas_calkowity = wez_czas_calkowity();

	for (auto it = wezly.begin(); it < wezly.end(); it++) {
		it->LF = czas_calkowity;
	}
}

