#pragma once

#include "node.h"

class graf
{
public:
	std::vector<node> wezly;
	int budzet; //dostepny budzet do przeznaczenia na ciecia kosztow
	int budzet_wydany;
	graf(void) : budzet(0) {}
	graf(int B) : budzet(B) {}
	~graf(void);

	void dodaj_wezel(int id, int czas);
	void dodaj_wezel(int id, int czas, int t_min, int koszt);
	void usun_wezel(int id); // usuwanie wezla z grafu
	void polacz(int src, int dst);
	int rozmiar();

	int wez_czas_calkowity();
	void przypisz_wszystkim_LF_max();
	
};