#include <iostream>
#include <fstream>
#include <sstream>
#include <list>
#include <cstdio>
#include <Windows.h>
#include <iomanip>
#include"graf.h"

using namespace std;

void wyswietl_wynik(graf G) {
	cout << "Calkowity czas dzialania\n " << G.wez_czas_calkowity() << endl;

	cout << "\nWyniki: nod, czas, ES, EF, LS, LF\n\n";

	for (auto it = G.wezly.begin(); it < G.wezly.end(); it++) {
		cout << setw(4) << it->id + 1 << ":" << setw(5) << it->czas_wykonania << "     ";
		cout << setw(5) << it->ES << setw(5) << it->EF << setw(5) << it->LS << setw(5) << it->LF << endl;
	}
}

void zapisz_wynik(graf G, string nazwa) {
	ofstream strm_wy(nazwa);

	strm_wy << "Calkowity czas dzialania\n " << G.wez_czas_calkowity() << endl;

	strm_wy << "\nWyniki: nod, czas, ES, EF, LS, LF\n\n";

	for (auto it = G.wezly.begin(); it < G.wezly.end(); it++) {
		strm_wy << setw(4) << it->id + 1 << ":" << setw(5) << it->czas_wykonania << "     ";
		strm_wy << setw(5) << it->ES << setw(5) << it->EF << setw(5) << it->LS << setw(5) << it->LF << endl;
	}

	strm_wy << "Wydano: " << G.budzet_wydany << endl;
	strm_wy.close();
}

void oblicz_ls_lf(graf *G, vector<int> permutacja)
{
	G->przypisz_wszystkim_LF_max();

	for (int i = G->rozmiar() - 1; i >= 0; i--) {
		G->wezly[permutacja[i]].oblicz_LS_obecnego();
		G->wezly[permutacja[i]].oblicz_LF_nastepnego(G);
	}
}



void oblicz_es_ef(graf *G, vector<int> permutacja)
{
	for (int i = 0; i < G->rozmiar(); i++) {
		G->wezly[permutacja[i]].oblicz_EF_obecnego();
		G->wezly[permutacja[i]].oblicz_ES_nastepnego(G);
	}
}

void top_sort(graf G, vector<int>& permutacja) {
	//znajdz wierzcholek startowy
	while (permutacja.size() < G.rozmiar()){
		for (int i = 0; i < G.rozmiar(); i++){
			if (G.wezly[i].krawedzie_we.empty() && G.wezly[i].id != -1){
				permutacja.push_back(G.wezly[i].id);
				G.usun_wezel(G.wezly[i].id);
			}
		}
	}
}

graf wczytaj_dane(string path) {
	int N = 0;	// ilosc wierzcholkow grafu
	int M = 0;	//ilosc wiezow
	int t; //czas wykonania

	graf G;
	ifstream in(path);

	if (in.is_open()) {
		in >> N >> M;
	}

	for (int i = 0; i < N; i++) {
		//wczytaj dane do grafu(wezly + czas wykonania)
		in >> t;
		G.dodaj_wezel(i, t);
	}

	//czytaj nastepna linie i dodawaj krawedze wchodzace do wezla
	int src, dst; // wezel wejsciowy i wyjsciowy krawedzi

	for (int i = 0; i < M; i++) {
		in >> src >> dst;
		G.polacz(src, dst);
	}

	in.close();

	return G;
}

graf wczytaj_dane_k(string path) {
	int N = 0;	// ilosc wierzcholkow grafu
	int M = 0;	//ilosc wiezow
	int B = 0;  //dostepny budzet
	int t; //czas wykonania
	int t_min; // minimalny czas wykonania
	int koszt; // jednostkowy koszt

	
	ifstream in(path);

	if (in.is_open()) {
		in >> N >> M >> B;
	}
	graf G(B);
	for (int i = 0; i < N; i++) {
		//wczytaj dane do grafu(wezly + czas wykonania)
		in >> t >> t_min >> koszt;
		G.dodaj_wezel(i, t, t_min, koszt);
	}

	//czytaj nastepna linie i dodawaj krawedze wchodzace do wezla
	int src, dst; // wezel wejsciowy i wyjsciowy krawedzi

	for (int i = 0; i < M; i++) {
		in >> src >> dst;
		G.polacz(src, dst);
	}

	in.close();

	return G;
}

void redukuj(graf& G, vector<int>& permutacja){ //proba zmniejszenia kosztu 
	int red = 999999;
	int i = 0;
	while (G.budzet > 0 && red > 0){//dopoki mamy fundusze
		//przejdz przez wszystkie wierzcholki w grafie
		for (auto it = G.wezly.begin(); it < G.wezly.end(); it++){
			if (it->ES == it->LS && it->EF == it->LF){ //element lezy na sciezce krytycznej
				//redukujemy czas maksymalnie
				red = min((G.budzet / it->koszt), it->czas_wykonania - it->min_czas_wykonania);
				it->inwestuj(red);
				G.budzet -= red*it->koszt;
				G.budzet_wydany += red*it->koszt;
			}
		}
		//przelicz parametry procesu ponownie
		oblicz_es_ef(&G, permutacja);
		oblicz_ls_lf(&G, permutacja);
	}
}

void executeProgram(string plik_we, string plik_wy) {
	vector<int> permutacja;
	graf G = wczytaj_dane_k(plik_we);

	top_sort(G, permutacja);

	oblicz_es_ef(&G, permutacja);
	oblicz_ls_lf(&G, permutacja);
	redukuj(G, permutacja);
	zapisz_wynik(G, plik_wy);
}



int main(void) {
	string plik_we;
	string plik_wy;

	for (int i = 1; i <= 8; i++) {
		stringstream strm;
		strm << "data" << i << "0.txt";
		plik_we = strm.str();
		strm.str(std::string());
		strm << "data" << i << "0 wynik.txt";
		plik_wy = strm.str();
		executeProgram(plik_we, plik_wy);
	}
}