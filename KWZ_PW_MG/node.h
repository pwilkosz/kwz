#pragma once

#include <vector>
#include <iostream>
#include <algorithm>

class graf;

class node
{
public:
	int id;
	int czas_wykonania, min_czas_wykonania, koszt, ES, EF, LS, LF;

	std::vector<int> krawedzie_we;
	std::vector<int> krawedzie_wy;

	node(int id, int czas_w);
	node(int id, int czas_w, int min_czas, int koszt);
	~node(void);

	void dodaj_krawedz_we(int id);
	void dodaj_krawedz_wy(int id);

	void oblicz_EF_obecnego();
	void oblicz_ES_nastepnego(graf *G);
	void zaproponuj_ES(int es);

	void oblicz_LS_obecnego();
	void oblicz_LF_nastepnego(graf *G);
	void zaproponuj_LF(int lf);

	void inwestuj(int il_jedn);
};
